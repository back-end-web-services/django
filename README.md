# Django

## Docker

### 1. Install Docker
If you're new to [Docker](https://docs.docker.com/installation/), you might also want to check out the [tutorial](https://docs.docker.com/userguide/dockerizing/).

### 2. Clone repo
```
$ git clone git@gitlab.com:back-end-web-services/django.git django
$ cd django
```
### 3. Allow the Docker network interface
```
$ sudo firewall-cmd --zone=public --add-masquerade --change-interface=docker0 --permanent
$ sudo firewall-cmd --reload
$ sudo systemctl restart docker
```
### 4. Build the image and migrate database:
```
$ docker-compose up -d --build
$ docker-compose exec web python manage.py makemigrations
$ docker-compose exec web python manage.py migrate
```

## Heroku

### 1. Sign up [Heroku](https://www.heroku.com/) and create an account.
### 2. Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).
### 3. Login Heroku and create a project.
```
$ heroku login
$ heroku create <app_name>
```
### 4. Add you Heroku Git repository and push your code.
```
$ heroku git:remote -a <app_name>
$ git push heroku master  # For production
$ git push heroku main  # For development
```
### 5. In step 4, Heroku will use `heroku.yml` to build a Docker container according to your `Dockerfile`.
### 6. Set your amount of Heroku's dynos.
```
$ heroku ps:scale web=1
```
### 7. Open your website.
```
$ heroku open
```