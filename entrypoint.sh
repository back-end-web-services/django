#!/bin/sh

# Run app with flags environment variable
# command="python manage.py runserver 127.0.0.1:8000"
command="python manage.py runserver 127.0.0.1:8000"
echo -----------------------------------------------------
echo $(date)
$command &
child=$!
# first wait will be interrupted be a signal
wait $child
# second wait is used to capture the return code
wait $child
exit_status=$?
timestamp=$(date +"%s")
echo end of child process with code $exit_status
# send abnormal exit status to /logs for monitoring
exit $exit_status
