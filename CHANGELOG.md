## [0.0.4] - 2020-11-09
### Features
- Update `Dockerfile` and it will generate `requirements.txt`.
- Install gunicorn to make a connection to Heroku.
- Install whitenoise to collect static files on Heroku.
- Install psycopg2 and psycopg2-binary to connect PostgreSQL database on Heroku.
- Install dj-database-url to get database information to Django on Heroku.
- Update `settings.py` and `wsgi.py` to apply Heroku's settings.
- Create `heroku.yml` to tell Heroku deploy actions.

## [0.0.3] - 2020-11-06
### Features
- Create Django REST APIs and the browsable API.
- Create simple test cases.
- Refactor all HTML files and apply official CSS to them.

## [0.0.2] - 2020-10-29
### Features
- Organize file structures and reuse templates.
- Apply `pipenv` to manage the Python packages.
- Apply `Dockerfile` to create an image for deployment.
- Apply `docker-compose.yml` to describe how the container works.

## [0.0.1] - 2020-10-27
### Features
- Users are able to log in thru `/accounts/login`.
- Users are able to log out thru `/accounts/logout`.
- Users can be created thru `/accounts/register`.
- Users can be changed thru `/accounts/password_change`.
- Users can be reset thru `/accounts/password_reset`.
- Create a simple home page `/home`

[0.0.4]: https://gitlab.com/back-end-web-services/django/-/releases/v0.0.4
[0.0.3]: https://gitlab.com/back-end-web-services/django/-/releases/v0.0.3
[0.0.2]: https://gitlab.com/back-end-web-services/django/-/releases/v0.0.2
[0.0.1]: https://gitlab.com/back-end-web-services/django/-/releases/v0.0.1
