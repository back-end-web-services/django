# Generated by Django 3.1.3 on 2020-11-06 12:35

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model_name', models.CharField(default='', help_text='the model name of DUT', max_length=200, verbose_name='Model Name')),
                ('serial_number', models.CharField(default='', help_text='the serial number of DUT', max_length=200, verbose_name='Serial Number')),
                ('sys_ip', models.CharField(default='', help_text='the system IP of SUT', max_length=200, verbose_name='IP')),
                ('sys_mac', models.CharField(default='', help_text='the system MAC of SUT', max_length=200, verbose_name='MAC')),
                ('create_time', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Create Time')),
                ('status', models.CharField(default='New', max_length=10, verbose_name='Status')),
                ('progress', models.CharField(default='0 %', help_text='the progress of execution', max_length=200, verbose_name='Progress')),
                ('result', models.CharField(default='On-going', help_text='the result of execution', max_length=200, verbose_name='Result')),
            ],
        ),
    ]
