from django.contrib import admin

# Register your models here.
from home.models import Submission


@admin.register(Submission)
class DutAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'create_time',
        'model_name', 'serial_number',
        'sys_ip', 'sys_mac',
        'status'
    )
    list_filter = (
        'id', 'create_time',
        'model_name', 'serial_number',
        'sys_ip', 'sys_mac',
        'status', 'progress', 'result'
    )
    fieldsets = (
        ('Universal Information', {'fields': ('id', 'create_time')}),
        ('DUT Information', {'fields': ('model_name', 'serial_number')}),
        ('Network Information', {'fields': ('sys_ip', 'sys_mac')}),
        ('Result', {'fields': ('status', 'progress', 'result')})
    )
    readonly_fields = (
        'id', 'create_time',
        'status', 'progress', 'result'
    )
