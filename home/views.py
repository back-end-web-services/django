from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView

from home.models import Submission
from home.serializers import SubmitSerializer
from rest_framework import generics
from rest_framework import permissions
from home.permissions import IsOwnerOrReadOnly
class HomePageView(LoginRequiredMixin, TemplateView):

    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['submission'] = Submission.objects.all()
        context['model'] = Submission._meta.get_fields()
        return context


class SubmitList(LoginRequiredMixin, generics.ListCreateAPIView):
    queryset = Submission.objects.all()
    serializer_class = SubmitSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class SubmitDetail(LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Submission.objects.all()
    serializer_class = SubmitSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
