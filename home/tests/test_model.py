from django.test import TestCase
# Create your tests here.
from home.models import Submission


class DemoTest(TestCase):
    """ Test module for Puppy model """

    def setUp(self):
        Submission.objects.create(
            model_name='Alpha', serial_number='TW0001',
            sys_ip='8.8.8.8', sys_mac='00:00:00:00:00:00:00:00')
        Submission.objects.create(
            model_name='Beta', serial_number='TW0001',
            sys_ip='8.8.8.8', sys_mac='00:00:00:00:00:00:00:00')

    def test(self):
        alpha = Submission.objects.get(model_name='Alpha')
        beta = Submission.objects.get(model_name='Beta')
        self.assertEqual(
            alpha.string, "This is alpha.")
        self.assertEqual(
            beta.string, "This is beta.")
