from accounts.models import CustomUser
from django.db import models
from django.utils import timezone


# Create your models here.
class Submission(models.Model):
    # SUT information.
    model_name = models.CharField(
        max_length=200, verbose_name='Model Name', help_text='the model name of DUT',
        default='')
    serial_number = models.CharField(
        max_length=200, verbose_name='Serial Number', help_text='the serial number of DUT',
        default='')

    # Network information.
    sys_ip = models.CharField(
        max_length=200, verbose_name='IP', help_text='the system IP of SUT',
        default='')
    sys_mac = models.CharField(
        max_length=200, verbose_name='MAC', help_text='the system MAC of SUT',
        default='')

    create_time = models.DateTimeField(default=timezone.now, verbose_name='Create Time')
    status = models.CharField(max_length=10, default='New', verbose_name='Status')

    # Result
    progress = models.CharField(
        max_length=200, verbose_name='Progress', help_text='the progress of execution',
        default='0 %')
    result = models.CharField(
        max_length=200, verbose_name='Result', help_text='the result of execution',
        default='On-going')

    owner = models.ForeignKey(
        CustomUser, related_name='submits',
        on_delete=models.CASCADE, null=True
    )

    @property
    def string(self):
        return 'This is {}.'.format(self.model_name.lower())
