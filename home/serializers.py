from home.models import Submission
from rest_framework import serializers


class SubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = [
            'id',
            'model_name',
            'serial_number',
            'sys_ip',
            'sys_mac',
            'owner',
        ]
    owner = serializers.ReadOnlyField(source='owner.username')
    # def create(self, validated_data):
    #     """Create and return a new `Snippet` instance, given the validated data."""
    #     return Submission.objects.create(**validated_data)

    # def update(self, instance, validated_data):
    #     """Update and return an existing `Snippet` instance, given the validated data."""
    #     instance.model_name = validated_data.get('model_name', instance.model_name)
    #     instance.serial_number = validated_data.get('serial_number', instance.serial_number)
    #     instance.sys_ip = validated_data.get('sys_ip', instance.sys_ip)
    #     instance.sys_mac = validated_data.get('sys_mac', instance.sys_mac)
    #     instance.save()
    #     return instance
