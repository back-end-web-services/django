from django.urls import path

from home.views import HomePageView, SubmitDetail, SubmitList

app_name = 'home'

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('submits/', SubmitList.as_view(), name='submits'),
    path('submits/<int:pk>/', SubmitDetail.as_view()),
]
