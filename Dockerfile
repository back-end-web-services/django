# Pull base image
FROM python:3.10.0-alpine

# Set work directory
ARG working_directory=/app
RUN mkdir -p ${working_directory}/src
WORKDIR ${working_directory}/src

# Install dependencies
RUN python -m pip install --upgrade pip
RUN pip install pipenv
COPY Pipfile Pipfile.lock /app/
RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt

# Copy project
COPY . ${working_directory}/src

CMD entrypoint.sh
