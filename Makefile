PROJECT="django"
COMPOSE_FILE="docker-compose.yml"

build:
	docker build -t jbl-${PROJECT}:latest -f ./Dockerfile .

run: build
	docker-compose -p jbl-$(PROJECT) -f $(COMPOSE_FILE) up -d

down:
	docker-compose -p jbl-$(PROJECT) -f $(COMPOSE_FILE) down

clean:
	docker-compose -p jbl-$(PROJECT) -f $(COMPOSE_FILE) down || true
	echo y | docker image prune -a || true

.PHONY: run clean build
