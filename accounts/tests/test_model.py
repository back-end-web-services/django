# Create your tests here.
from accounts.models import CustomUser
from django.test import TestCase


class DemoTest(TestCase):
    """ Test module for Puppy model """

    def setUp(self):
        CustomUser.objects.create(
            username='Alpha', password='alpha',
            email='alpha@demo.com', first_name='Alpha', last_name='Go')
        CustomUser.objects.create(
            username='Beta', password='beta',
            email='beta@demo.com', first_name='Beta', last_name='Go')

    def test(self):
        alpha = CustomUser.objects.get(username='Alpha')
        beta = CustomUser.objects.get(username='Beta')
        self.assertEqual(alpha.full_name, "Alpha Go")
        self.assertEqual(beta.full_name, "Beta Go")
