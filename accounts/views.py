from django.shortcuts import render
# Create your views here.
from django.urls import reverse_lazy as _
from django.views.generic.edit import CreateView
from rest_framework import generics

from accounts.models import CustomUser

from .forms import CustomUserCreationForm
from .serializers import UserSerializer


class RegisterView(CreateView):
    form_class = CustomUserCreationForm
    success_url = _('login')
    template_name = 'registration/register.html'


class UserList(generics.ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
