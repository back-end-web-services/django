from home.models import CustomUser, Submission
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    submits = serializers.PrimaryKeyRelatedField(many=True, queryset=Submission.objects.all())

    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'submits']
