from django.urls import path

from .views import RegisterView, UserDetail, UserList

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('users/', UserList.as_view(), name='users'),
    path('users/<int:pk>/', UserDetail.as_view()),
]
